The Harris Firm is a law firm that helps individuals throughout Alabama mainly in the areas of bankruptcy, family law, divorce, probate, and injury. Many of these cases are worked on a retainer basis or contingency fee.

Address: 4000 Eagle Point Corporate Dr, #110, Birmingham, AL 35242, USA
Phone: 205-201-1789
Website: https://www.theharrisfirmllc.com/birminghamdivorce/
